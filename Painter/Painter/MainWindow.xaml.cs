﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Painter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Brush color;
        private bool isDraw;
        private double r;

        public MainWindow()
        {
            InitializeComponent();
            isDraw = false;
            color = Brushes.Red;
            rbRed.IsChecked = true;
            r = 1;
        }

        private void cmDown(object sender, MouseButtonEventArgs e)
        {
            isDraw = true;
        }

        private void cmUp(object sender, MouseButtonEventArgs e)
        {
            isDraw = false;
        }

        private void cmMove(object sender, MouseEventArgs e)
        {
            if (isDraw)
            {
                if ((e.GetPosition(g).X < 0) || (e.GetPosition(g).X > g.ActualWidth))
                {
                    return;
                }

                if ((e.GetPosition(g).Y < 0) || (e.GetPosition(g).Y > g.ActualHeight))
                {
                    return;
                }

                Ellipse O = new Ellipse();
                O.Width = r;
                O.Height = r;
                O.Fill = color;
                O.Margin = new Thickness(e.GetPosition(g).X - r / 2, e.GetPosition(g).Y - r / 2, 0, 0);
                g.Children.Add(O);
            }
        }

        private void cmClear(object sender, RoutedEventArgs e)
        {
            g.Children.Clear();
        }

        private void cmRed(object sender, RoutedEventArgs e)
        {
            color = Brushes.Red;
            rColor.Fill = color;
        }

        private void cmBlue(object sender, RoutedEventArgs e)
        {
            color = Brushes.Blue;
            rColor.Fill = color;
        }

        private void cmGreen(object sender, RoutedEventArgs e)
        {
            color = Brushes.Green;
            rColor.Fill = color;
        }

        private void cmSize(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            r = slSize.Value;
        }
    }
}
